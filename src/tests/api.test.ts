import '@testing-library/jest-dom/extend-expect'

import {todoService} from '../services/todo.service'


test('test API calls', async () => {
    const todoOne = await todoService.getSingle(1);
    const todos = await todoService.getCollection();


    expect(todoOne.id).toBe(1);
    expect(todoOne.userId).toBe(1);
    expect(todoOne.completed).toBeFalsy();
    expect(todoOne.title).toBe("delectus aut autem");
    expect(Array.isArray(todos)).toBeTruthy();
    expect(todos.length).toBe(200);


});