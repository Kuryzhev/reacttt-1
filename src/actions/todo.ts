import {Todo, todoConstants} from '../constants/todo';
import {todoService} from '../services/todo.service';
import {Dispatch} from "redux";

export const todoActions = {
    getSingle,
    getCollection,
};

function getSingle(id: number | string) {
    return (dispatch: Dispatch) => {
        dispatch(request(id));

        todoService.getSingle(id)
            .then(
                (todo: Todo) => {
                    dispatch(success(todo));
                },
                (error: any) => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(id: number | string) {
        return {type: todoConstants.TODO_SINGLE_REQUEST, id}
    }

    function success(todo: Todo) {
        return {type: todoConstants.TODO_SINGLE_SUCCESS, item: todo}
    }

    function failure(error: any) {
        return {type: todoConstants.TODO_SINGLE_ERROR, error}
    }
}

function getCollection() {
    return (dispatch: Dispatch) => {
        dispatch(request());

        todoService.getCollection()
            .then(
                (todos: Todo[]) => {
                    dispatch(success(todos));
                },
                (error: any) => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request() {
        return {type: todoConstants.TODO_COLLECTION_REQUEST}
    }

    function success(todos: Todo[]) {
        return {type: todoConstants.TODO_COLLECTION_SUCCESS, collection: todos}
    }

    function failure(error: any) {
        return {type: todoConstants.TODO_COLLECTION_ERROR, error}
    }
}

