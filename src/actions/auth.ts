import { authConstants } from '../constants/auth';
import { authService } from '../services/auth.service';
import {Dispatch} from 'redux';

export const authActions = {
    login,
    logout,
};

function login(username: string) {
    return (dispatch: Dispatch) => {
        dispatch(request({ username }));

        authService.login(username)
            .then(
                (user: any) => {
                    dispatch(success(user));
                },
                (error: any) => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(user: any) { return { type: authConstants.LOGIN_REQUEST, user } }
    function success(user: any) { return { type: authConstants.LOGIN_SUCCESS, user } }
    function failure(error: any) { return { type: authConstants.LOGIN_FAILURE, error } }
}

function logout() {
    authService.logout();
    return { type: authConstants.LOGOUT };
}

