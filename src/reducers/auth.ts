import {AuthActionTypes, authConstants, AuthRequestAction, AuthSuccessAction} from '../constants/auth';

const userRecord = localStorage.getItem('user');
let user = userRecord ? JSON.parse(userRecord) : null;
const initialState = user ? {loggedIn: true, user} : {};

export function authentication(state = initialState, action: AuthActionTypes) {
    switch (action.type) {
        case authConstants.LOGIN_REQUEST:
            return {
                loggingIn: true,
                user: (action as AuthRequestAction).user
            };
        case authConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: (action as AuthSuccessAction).user
            };
        case authConstants.LOGIN_FAILURE:
            return {};
        case authConstants.LOGOUT:
            return {};
        default:
            return state
    }
}