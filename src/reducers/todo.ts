import {
    TodoActionTypes,
    todoConstants,
    CollectionTodoErrorAction,
    CollectionTodoSuccessAction,
    SingleTodoErrorAction, SingleTodoSuccessAction
} from "../constants/todo";


const initialState = {
    collection: {fetching: false, error: null, collection: null},
    single: {fetching: false, error: null, item: null}
};

export function todo(state = initialState, action: TodoActionTypes) {
    switch (action.type) {
        case todoConstants.TODO_COLLECTION_REQUEST:
            return {
                ...state,
                collection: {fetching: true, error: null, collection: null}
            };
        case todoConstants.TODO_COLLECTION_ERROR:
            return {
                ...state,
                collection: {fetching: false, error: (action as CollectionTodoErrorAction).error, collection: null}
            };
        case todoConstants.TODO_COLLECTION_SUCCESS:
            return {
                ...state,
                collection: {fetching: false, error: null, collection: (action as CollectionTodoSuccessAction).collection}
            };
        case todoConstants.TODO_SINGLE_REQUEST:
            return {
                ...state,
                single: {fetching: true, error: null, item: null}
            };
        case todoConstants.TODO_SINGLE_ERROR:
            return {
                ...state,
                single: {fetching: false, error: (action as SingleTodoErrorAction).error, item: null}
            };
        case todoConstants.TODO_SINGLE_SUCCESS:
            return {
                ...state,
                single: {fetching: false, error: null, item: (action as SingleTodoSuccessAction).item}
            };
        default:
            return state
    }
}