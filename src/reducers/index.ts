import {combineReducers} from 'redux';
import {authentication} from './auth';
import {todo} from "./todo";

export default combineReducers({
    authentication,  todo
});