export const todoConstants = {
    TODO_SINGLE_SUCCESS: 'TODO_SINGLE_SUCCESS',
    TODO_SINGLE_ERROR: 'TODO_SINGLE_ERROR',
    TODO_SINGLE_REQUEST: 'TODO_SINGLE_REQUEST',

    TODO_COLLECTION_SUCCESS: 'TODO_COLLECTION_SUCCESS',
    TODO_COLLECTION_ERROR: 'TODO_COLLECTION_ERROR',
    TODO_COLLECTION_REQUEST: 'TODO_COLLECTION_REQUEST',
};

export class Todo {
    public userId!: number;
    public id!: number;
    public title!: string;
    public completed!: boolean;
}

export interface SingleTodoSuccessAction {
    type: typeof todoConstants.TODO_SINGLE_SUCCESS
    item: Todo
}

export interface SingleTodoErrorAction {
    type: typeof todoConstants.TODO_SINGLE_ERROR
    error: string
}

export interface SingleTodoRequestAction {
    type: typeof todoConstants.TODO_SINGLE_REQUEST,
}


export interface CollectionTodoSuccessAction {
    type: typeof todoConstants.TODO_COLLECTION_SUCCESS
    collection: Todo[]
}

export interface CollectionTodoErrorAction {
    type: typeof todoConstants.TODO_COLLECTION_ERROR
    error: string
}

export interface CollectionTodoRequestAction {
    type: typeof todoConstants.TODO_COLLECTION_REQUEST,
}

export type TodoActionTypes = SingleTodoSuccessAction | SingleTodoErrorAction | SingleTodoRequestAction
| CollectionTodoSuccessAction | CollectionTodoErrorAction | CollectionTodoRequestAction