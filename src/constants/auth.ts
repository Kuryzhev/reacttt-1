export const authConstants = {
    LOGIN_REQUEST: 'AUTH_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'AUTH_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'AUTH_LOGIN_FAILURE',

    LOGOUT: 'AUTH_LOGOUT',
};


export interface AuthRequestAction {
    type: typeof authConstants.LOGIN_REQUEST
    user: any,
}

export interface AuthSuccessAction {
    type: typeof authConstants.LOGIN_SUCCESS
    user: any,
}

export interface AuthFailureAction {
    type: typeof authConstants.LOGIN_FAILURE,
    error: any
}

export interface AuthClearAction {
    type: typeof authConstants.LOGOUT,
}

export type AuthActionTypes = AuthRequestAction | AuthSuccessAction | AuthFailureAction | AuthClearAction