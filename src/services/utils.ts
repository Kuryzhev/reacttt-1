export async function handleResponse<T>(response: Response): Promise<T> {
    const text = await response.text();
    const data = text && JSON.parse(text);
    if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
    }

    return data;

}