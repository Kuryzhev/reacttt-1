
export const authService = {
    login,
    logout,
};

async function login(username: string): Promise<string> {
    sessionStorage.setItem('username', username);
    return username;
}

function logout() {
    localStorage.removeItem('username');
}





