import config from '../config';
import {Todo} from "../constants/todo";
import {handleResponse} from "./utils";

export const todoService = {
    getSingle,
    getCollection,
};

async function getSingle(id: string | number): Promise<Todo> {
    const requestOptions = {
        method: 'GET',
    };

    const response = await fetch(`${config.get('apiUrl')}/todos/${id}`, requestOptions);
    return handleResponse<Todo>(response);

}

async function getCollection(): Promise<Todo[]> {
    const requestOptions = {
        method: 'GET',
    };

    const response = await fetch(`${config.get('apiUrl')}/todos`, requestOptions);
    return handleResponse<Todo[]>(response);
}

