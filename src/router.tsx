import React from "react";
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import Todo from './components/todo/Todo'
import Login from './components/login/Login'
import Todos from './components/todos/Todos'


function AppRouter() {
    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                        <li>
                            <Link to="/todos/1">First todo</Link>
                        </li>
                    </ul>
                </nav>
                <br/>
                <Route path="/" exact component={Todos}/>
                <Route path="/todos/:id" component={Todo}/>
                <Route path="/login" component={Login}/>
            </div>
        </Router>
    );
}

export default AppRouter;
