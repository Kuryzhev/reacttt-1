import React, {FormEvent} from "react";
import {Button, FormGroup, FormControl, FormLabel} from "react-bootstrap";
import {connect} from 'react-redux';
import {authActions} from '../../actions/auth';
import "./Login.css";
import {Redirect} from "react-router-dom"

class LoginComponent extends React.Component<any, any> {
    state = {username: ''};


    validateForm() {
        return this.state.username.length > 0;
    }

    componentDidMount(): void {
        const username = sessionStorage.getItem('username');
        const {dispatch} = this.props;
        if (username) {
            dispatch(authActions.login(username));
        }
    }

    setUsername(username: string) {
        this.setState((state: any) => {
            return {username};
        })
    }

    handleSubmit(event: FormEvent<HTMLElement>) {
        event.preventDefault();
        const {dispatch} = this.props;
        if (this.state.username) {
            dispatch(authActions.login(this.state.username));
        }
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div>
                {
                    this.props.loggedIn ? <Redirect to="/"/> : (<div className="Login">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <FormGroup controlId="email">
                                <FormLabel>Email</FormLabel>
                                <FormControl
                                    autoFocus
                                    type="email"
                                    value={this.state.username}
                                    onChange={(e: FormEvent<HTMLInputElement>) => this.setUsername((e.target as HTMLInputElement).value)}
                                />
                            </FormGroup>
                            <Button block size="lg" disabled={!this.validateForm()} type="submit">
                                Login
                            </Button>
                        </form>
                    </div>)
                }
            </div>
        );
    }


}

function mapStateToProps(state: any) {
    const {loggingIn, loggedIn} = state.authentication;
    return {
        loggingIn, loggedIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginComponent);
export default connectedLoginPage;
