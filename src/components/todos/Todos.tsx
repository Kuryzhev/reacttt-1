import React from "react";
import {connect} from "react-redux";
import {Todo} from "../../constants/todo";
import {todoActions} from "../../actions/todo";
import {Redirect} from "react-router-dom"

import './Todos.css'
import {Link} from "react-router-dom";

class Todos extends React.Component<any, any> {
    componentDidMount(): void {
        this.fetchTodos();
    }

    fetchTodos() {
        this.props.dispatch(todoActions.getCollection())
    }


    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div className="App">
                {
                    this.props.loggedIn ? (<div className="todos">
                        {this.props.error ? <h1>{this.props.error}</h1> : null}
                        {this.props.fetching ? <h1>Fetching todos...</h1> : null}
                        {this.props.collection ? this.props.collection.map((item: Todo) => (
                            <div className={item.completed ? 'completed' : 'uncompleted'}>
                                <input type="checkbox" checked={item.completed}/>
                                <Link to={`/todos/${item.id}`}> {item.id}. {item.title}</Link>
                            </div>
                        )) : null}
                    </div>) : <Redirect to="/login"/>
                }

            </div>
        );
    }
}


function mapStateToProps(state: any) {
    const {collection: {fetching, error, collection}} = state.todo;
    const {loggedIn} = state.authentication;
    return {
        fetching, error, collection, loggedIn
    };
}

const connectedTodosPage = connect(mapStateToProps)(Todos);
export default connectedTodosPage;
