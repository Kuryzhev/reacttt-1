import React from "react";
import {todoActions} from "../../actions/todo";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

class TodoComponent extends React.Component<any, any> {
    componentDidMount(): void {
        this.fetchTodo();
    }

    fetchTodo() {
        const query = this.props.location.pathname.split('/');
        this.props.dispatch(todoActions.getSingle(query[query.length - 1]));
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            this.props.loggedIn ? (
                <div className="todo">
                    {this.props.error ? <h1>{this.props.error}</h1> : null}
                    {this.props.fetching ? <h1>Fetching item...</h1> : null}
                    {this.props.item ? (
                        <div className={this.props.item.completed ? 'completed' : 'uncompleted'}>
                            <h1>{this.props.item.title}</h1>
                            <p>{this.props.item.completed ? 'Completed' : 'Not completed'}</p>
                            <p>Id: {this.props.item.id}. </p>
                            <p>User id: {this.props.item.userId}</p>
                        </div>
                    ) : null}
                </div>) : <Redirect to="/login"/>
        );
    }
}


function mapStateToProps(state: any) {
    const {single: {fetching, error, item}} = state.todo;
    const {loggedIn} = state.authentication;
    return {
        fetching, error, item, loggedIn
    };
}

const connectedTodoPage = connect(mapStateToProps)(TodoComponent);
export default connectedTodoPage;
