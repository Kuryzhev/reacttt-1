import config from 'react-global-configuration';

config.set({ apiUrl: 'https://jsonplaceholder.typicode.com'});

export default config;
